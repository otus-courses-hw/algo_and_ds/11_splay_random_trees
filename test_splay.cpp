#include <catch2/catch.hpp>

#include "tree_base.hpp"
#include "splay_tree.hpp"
#include "util.hpp"

TEMPLATE_PRODUCT_TEST_CASE("rotations", "[splay_base]", (tree_base), ((int, int)))
{
    TestType tree;
    std::vector keys{10,5,0,7,15,12,17};
    for (const auto &key : keys)
        tree.just_insert(key, 666);
    /*          BEFORE:
     *              10
     *             /  \
     *            5    15
     *           / \   / \
     *          0  7  12  17
     */
    SECTION("small right rotation")
    {
        auto node = tree.search(15);
        auto tmp = tree.right_small_rotation(node);
        /*
         *          AFTER:
         *           15
         *          /  \
         *         10   17
         *        /  \
         *        5   12
         */
        CHECK(tree.m_root->m_key == 15);
        CHECK(tree.search(15) == tree.m_root);
        CHECK(tree.search(15)->m_left->m_key == 10);
        CHECK(tree.search(15)->m_right->m_key == 17);
        CHECK(tree.search(10)->m_parent->m_key == 15);
        CHECK(tree.search(17)->m_parent->m_key == 15);
        CHECK(tree.search(10)->m_left->m_key == 5);
        CHECK(tree.search(10)->m_right->m_key == 12);
        CHECK(tree.search(12)->m_parent->m_key == 10);
        CHECK(tree.search(5)->m_parent->m_key == 10);
    }

    SECTION("small left rotation")
    {
        auto node = tree.search(5);
        auto tmp = tree.left_small_rotation(node);
        /*            AFTER:
         *               5
         *             /  \
         *            0    10
         *                /  \
         *               7    15
         */
        CHECK(tree.m_root->m_key == 5);
        CHECK(tree.search(5) == tree.m_root);
        CHECK(tree.search(5)->m_left->m_key == 0);
        CHECK(tree.search(5)->m_right->m_key == 10);
        CHECK(tree.search(0)->m_parent->m_key == 5);
        CHECK(tree.search(10)->m_parent->m_key == 5);
        CHECK(tree.search(10)->m_left->m_key == 7);
        CHECK(tree.search(10)->m_right->m_key == 15);
        CHECK(tree.search(7)->m_parent->m_key == 10);
        CHECK(tree.search(15)->m_parent->m_key == 10);
    }
}

TEMPLATE_PRODUCT_TEST_CASE("insertions", "[splay]", (splay_tree), ((int, int)))
{
   TestType tree;
   tree.insert(10, 666);
   auto root = tree.root();
   /*     AFTER:
    *       10
    */
   CHECK(root->m_key == 10);

   tree.insert(20, 666);
   root = tree.root();
   /*       AFTER:
    *       20
    *      /
    *     10
    */
   CHECK(root->m_key == 20);
   CHECK(root->m_left->m_key == 10);

   tree.insert(5, 666);
   root = tree.root();
   /*       AFTER:
    *        5
    *          \
    *           20
    *          /
    *         10
    */
   CHECK(root->m_key == 5);
   CHECK(root->m_left == nullptr);
   CHECK(root->m_right->m_key == 20);
   CHECK(root->m_right->m_parent->m_key == 5);
   CHECK(root->m_right->m_right == nullptr);
   CHECK(root->m_right->m_left->m_key == 10);
   CHECK(root->m_right->m_left->m_parent->m_key == 20);
}