all: test

test: test_main.o test_splay.o
	g++ -g -O0 -std=c++20 -o $@ $^

%.o: %.cpp
	g++ -g -Werror -O0 -std=c++20 -o $@ -c $<

test_splay.o: test_main.cpp random_tree.hpp tree_base.hpp splay_tree.hpp util.hpp

.PHONY: clean run

clean:
	rm *.o test

run:
	./test
