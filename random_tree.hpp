#pragma once

#include "tree_base.hpp"
#include "splay_tree.hpp"
#include "util.hpp"

template <typename K, typename V>
class random_tree : private tree_base<K,V>, private splay_tree<K,V> {
private:
    template <typename N, typename M> using tree_base_t = tree_base<N,M>;
    template <typename N, typename M> using splay_tree_t = splay_tree<N,M>;
    template <typename N, typename M> using node_t = typename tree_base_t<N,M>::node_t;

    std::size_t m_size;

public:
    node_t<K,V> *root()
    {
        return tree_base_t<K,V>::m_root;
    }

    void insert(const K &key, const V &value)
    {
        if (util::dice(m_size) % m_size == 0)
            splay_tree_t<K,V>::insert(key, value);
        else
            tree_base_t<K,V>::just_insert(key, value);

        ++m_size;
    }
};