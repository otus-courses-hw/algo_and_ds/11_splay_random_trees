#pragma once

#include "tree_base.hpp"

template <typename K, typename V>
class splay_tree : private tree_base<K,V> {
private:
    template <typename N, typename M> using tree_t = tree_base<N,M>;
    template <typename N, typename M> using node_t = typename tree_t<N,M>::node_t;

public:
    node_t<K,V> *root()
    {
        return tree_t<K,V>::m_root;
    }

    void insert(const K &key, const V &value)
    {
        auto node = tree_t<K,V>::just_insert(key, value);
        if (node->m_key == root()->m_key)
            return;

        while (node != root())
        {
            if (node->m_parent->m_right == node)
                node = tree_t<K,V>::right_small_rotation(node);
            else
                node = tree_t<K,V>::left_small_rotation(node);
        }
    }
};