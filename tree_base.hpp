#pragma once

#include <utility>

template <typename K, typename V>
class tree_base
{
    public:
        tree_base() : m_root(nullptr) {}

        struct node_t
        {
            K m_key;
            V m_value;
            node_t *m_left;
            node_t *m_right;
            node_t *m_parent;

            node_t() = delete;
            node_t(K key, V value, node_t *parent) : m_key(key), m_value(value), m_left(nullptr), m_right(nullptr), m_parent(parent) {}
            ~node_t() = default;
        };

        node_t *m_root;

        node_t *just_insert(const K &key, const V &value)
        {
            return helper_insert(m_root, m_root, key, value);
        }

        node_t *search(const K &key)
        {
            auto node = helper_search(m_root, key);
            return node;
        }

        node_t *right_small_rotation(node_t *&node)
        {
            /*
             *              10
             *             /  \
             *            l    15
             *                 / \
             *                 c  r
             *
             *           15
             *          /  \
             *         10   r
             *        /  \
             *        l   c
             */
            if (node->m_parent != nullptr)
            {
                if (node->m_parent == m_root)
                    m_root = node;

                auto center = node->m_left; // just for readability sake

                //move center
                node->m_parent->m_right = node->m_left;
                node->m_left = node->m_parent;

                //move parents
                node->m_parent = node->m_parent->m_parent;
                node->m_left->m_parent = node;

                if (center != nullptr)
                    center->m_parent = node->m_left;

                return node;
            }
            else
                return nullptr;
        }

        node_t *left_small_rotation(node_t *&node)
        {
            /*
             *              10
             *             /  \
             *            5    r
             *          /  \
             *          l   c
             *
             *           5
             *         /  \
             *         l   10
             *            /  \
             *           c    r
             */
            if (node->m_parent != nullptr)
            {
                if (node->m_parent == m_root)
                    m_root = node;

                auto center = node->m_right; // just for readability sake

                //move center
                node->m_parent->m_left = node->m_right;
                node->m_right = node->m_parent;

                //move parents
                node->m_parent = node->m_parent->m_parent;
                node->m_right->m_parent = node;

                if (center != nullptr)
                    center->m_parent = node->m_right;

                return node;
            }
            else
                return nullptr;
        }


    private:
        node_t *helper_insert(node_t *&node, node_t *&parent, const K &key, const V &value)
        {
            if (node == nullptr)
            {
                node = new node_t(key, value, parent);
                return node;
            }

            if (node->m_key == key)
            {
                node->m_value = value;
                return node;
            }
            else if (node->m_key > key)
            {
                return helper_insert(node->m_left, node, key, value);
            }
            else
            {
                return helper_insert(node->m_right, node, key, value);
            }
        }

        node_t *helper_search(node_t *node, const K &key)
        {
            while (node != nullptr)
            {
                if (node->m_key == key)
                    return node;

                if (node->m_key > key)
                    node = node->m_left;
                else
                    node = node->m_right;
            }

            return nullptr;
        }

};
