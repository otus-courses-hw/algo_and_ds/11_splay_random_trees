#include <cstddef>
#include <utility>
#include <vector>
#include <numeric>
#include <random>
#include <algorithm>
#include <cassert>
#include <cstring>

namespace ranges = std::ranges;

namespace util
{
    namespace 
    {
        template <typename T> std::vector<T> container;

        template <typename T>
        void init_container(std::size_t n)
        {
            container<T>.resize(n, T());
        }
    }

    template <typename T>
    void do_shuffle(std::size_t n)
    {
        init_container<T>(n);
        std::iota(container<T>.begin(), container<T>.end(), 1);

        static std::random_device rd;
        static std::mt19937 gen(rd());

        ranges::shuffle(container<T>, gen);
    }

    template <typename T>
    void do_sort(std::size_t n, const char *order)
    {
        //take in mind that keys extracted from back()
        if (strcmp(order, "asc") == 0)
            std::iota(container<int>.rbegin(), container<int>.rend(), 1);
        else if (strcmp(order, "desc") == 0)
            std::iota(container<int>.begin(), container<int>.end(), 1);
    }

    template <typename T>
    T get_key()
    {
        assert(!container<int>.empty() && "there are not any keys");
        auto val = container<T>.back();
        container<T>.pop_back();
        return val;
    }

    std::size_t dice(std::size_t n)
    {
        static std::random_device rd;
        static std::mt19937 gen(rd());
        std::uniform_int_distribution<std::size_t> distrib(0, n);
        return distrib(gen);
    }
}
